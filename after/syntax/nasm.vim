" Spell check comments only (add contains=@Spell)
syn region  nasmComment		start=";" keepend end="$" contains=@nasmGrpInComments,@Spell
syn cluster nasmGrpInComments	contains=nasmInCommentTodo,@Spell
syn cluster nasmGrpComments	contains=@nasmGrpInComments,nasmComment,nasmSpecialComment,@Spell
