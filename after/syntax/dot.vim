" Spell check comments only
syn region   dotComment start="//" keepend end="$" contains=dotComment,dotTodo,@Spell
syn region   dotComment start="/\*" keepend end="\*/" contains=dotComment,dotTodo,@Spell
