" Enable indentLine
let g:indentLine_enabled = 1

" Customization
let g:indentLine_showFirstIndentLevel = 1
let g:indentLine_first_char = '┆'
let g:indentLine_char = '┆'
let g:indentLine_indentLevel = 64

"let g:indentLine_leadingSpaceEnabled = 1
"let g:indentLine_leadingSpaceChar = '·'

" Exclude json from conceal
autocmd FileType json,jsonc,markdown let g:indentLine_setConceal = 0 |
			\ setlocal conceallevel=0
