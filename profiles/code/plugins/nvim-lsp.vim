" ============== SECTION LUA ==============
lua << EOF
local lspconfig = require('lspconfig')

lspconfig.dotls.setup {
  on_attach = common_attach,
  capabilities = capabilities
}

lspconfig.html.setup {
  on_attach = ccls_attach,
  capabilities = capabilities
}

lspconfig.jsonls.setup {
  commands = {
    Format = {
      function()
        vim.lsp.buf.range_formatting({},{0,0},{vim.fn.line("$"),0})
      end
    }
  },
  on_attach = ccls_attach,
  capabilities = capabilities
}

lspconfig.cssls.setup {
  on_attach = ccls_attach,
  capabilities = capabilities
}
EOF
