" [profiles/code/plug.list.vim]
" Author: Adam Maleszka
" Description: This is a list of "code" profile's plugins.

" ============== APPEARANCE ==============

" ==== ==== Syntax ==== ====
" vim-jsonc - json c-style highlight support (c-style comments)
Plug 'kevinoid/vim-jsonc'

" ============== BEHAVIOR ==============

" ==== ==== Typing ==== ====
" vim-surround - "surroundings" manager
Plug 'tpope/vim-surround'
