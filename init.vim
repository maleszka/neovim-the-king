" File: init.vim
" Author: Adam Maleszka
" Description: Start point of Vim's configuration, sets up VimLoader

" Styles:

" ==== ==== ==== ==== TOP SECTION ==== ==== ==== ====

" ==== ==== ==== High Section ==== ==== ====

" ==== ==== Middle section ==== ====

" ==== Low section ====

" -- Sub section --



" ==== ==== Paths ==== ====

let g:config_home = resolve(expand('<sfile>:p:h')) . '/'

let g:vload_path = g:config_home . 'plugged/vload'


" ==== ==== VimLoader setup ==== ====

" -- Module dirs --

let g:module_dirs = [ g:config_home . 'modules', g:config_home . 'modules/plugins', g:config_home . 'modules/ft', g:config_home . 'modules/local' ]

" install dep
let g:module_dirs += [g:config_home . 'modules/install']

" -- Configuration --

" This option enables logging
let g:vload#log_file = "/tmp/vload.log"

" Show only errors
let g:vload#verbosity = 1

" Use small weight
if getenv("NVIM_VLOAD_WEIGHT") == v:null
  let g:vload#weight = 1
else
  let g:vload#weight = str2nr(getenv("NVIM_VLOAD_WEIGHT"))
endif


" -- Execution --

let &runtimepath .= ',' . g:vload_path

lua require('vload').setup(vim.g.module_dirs, vim.g['vload#weight'])
