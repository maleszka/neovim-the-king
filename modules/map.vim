" Author: Adam Maleszka
" Description: Set of generic, global mappings

" Mod info
let g:Module_map_type = 'global'
let g:Module_map_priority = 5
let g:Module_map_requires = []

function! g:Module_map_load()
  " Switch windows with alt
  "tnoremap <Esc> <C-\><C-n>
  tnoremap <A-h> <C-\><C-n><C-w>h
  tnoremap <A-j> <C-\><C-n><C-w>j
  tnoremap <A-k> <C-\><C-n><C-w>k
  tnoremap <A-l> <C-\><C-n><C-w>l
  inoremap <A-h> <C-\><C-n><C-w>h
  inoremap <A-j> <C-\><C-n><C-w>j
  inoremap <A-k> <C-\><C-n><C-w>k
  inoremap <A-l> <C-\><C-n><C-w>l
  nnoremap <A-h> <C-w>h
  nnoremap <A-j> <C-w>j
  nnoremap <A-k> <C-w>k
  nnoremap <A-l> <C-w>l

  " Map <C-L> (redraw screen) to also turn off search highlighting until the
  " next search
  nnoremap <C-L> :nohl<CR><C-L>

  " Format paragraph(s) to the current wrap mode
  nnoremap <silent> Q gqap
  xnoremap <silent> Q gq
  nnoremap <silent> <leader>Q ggVGgq
endfunction
