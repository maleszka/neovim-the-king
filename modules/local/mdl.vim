" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Check markdown files with mdl.

" Mod info
let g:Module_mdl_type = 'local'
let g:Module_mdl_priority = 3
let g:Module_mdl_filetypes = ['markdown']
let g:Module_mdl_requires = ['ale']

function! g:Module_mdl_load(bufnr)
  call setbufvar(a:bufnr, 'ale_linters', getbufvar(a:bufnr, 'ale_linters', []) + ['mdl'])
endfunction

function! g:Module_mdl_unload(bufnr)
  let lint_list = getbufvar(a:bufnr, 'ale_linters', [])
  call remove(lint_list, index(lint_list, 'mdl'))
endfunction
