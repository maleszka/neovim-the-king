" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Check cpp files using cpplint with ALE

" Mod info
let g:Module_cpplint_type = 'local'
let g:Module_cpplint_priority = 3
let g:Module_cpplint_filetypes = ['cpp']
let g:Module_cpplint_requires = ['ale']

function! g:Module_cpplint_load(bufnr)
  call setbufvar(a:bufnr, 'ale_linters', getbufvar(a:bufnr, 'ale_linters', []) + ['cpplint'])
endfunction

function! g:Module_cpplint_unload(bufnr)
  let lint_list = getbufvar(a:bufnr, 'ale_linters', [])
  call remove(lint_list, index(lint_list, 'cpplint'))
endfunction
