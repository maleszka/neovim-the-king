" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Check prose with proselint

" Mod info
let g:Module_proselint_type = 'local'
let g:Module_proselint_priority = 2
let g:Module_proselint_filetypes = ['markdown', 'tex']
let g:Module_proselint_requires = ['ale']

function! g:Module_proselint_load(bufnr)
  call setbufvar(a:bufnr, 'ale_linters', getbufvar(a:bufnr, 'ale_linters', []) + ['proselint'])
endfunction

function! g:Module_proselint_unload(bufnr)
  let lint_list = getbufvar(a:bufnr, 'ale_linters', [])
  call remove(lint_list, index(lint_list, 'proselint'))
endfunction
