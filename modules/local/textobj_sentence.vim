" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Make Neovim detect sentences more accurately

" Mod info
let g:Module_textobj_sentence_type = 'local'
let g:Module_textobj_sentence_priority = 4
let g:Module_textobj_sentence_filetypes = ['asciidoc', 'markdown', 'tex']
let g:Module_textobj_sentence_requires = ['plug_list']

function! g:Module_textobj_sentence_load(bufnr)
  " ==== Load ====
  call plug#load(['vim-textobj-user', 'vim-textobj-sentence'])

  " ==== Initialize ====
  call textobj#sentence#init()
endfunction

function! g:Module_textobj_sentence_unload(bufnr)
endfunction
