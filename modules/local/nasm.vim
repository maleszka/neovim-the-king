" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Netwide assembly configuration

" Mod info
let g:Module_nasm_type = 'local'
let g:Module_nasm_priority = 4
let g:Module_nasm_filetypes = ['nasm']
let g:Module_nasm_requires = ['ale']

function! g:Module_nasm_load(bufnr)
  call setbufvar(a:bufnr, 'ale_linters', getbufvar(a:bufnr, 'ale_linters', []) + ['nasm'])
endfunction

function! g:Module_nasm_unload(bufnr)
  let lint_list = getbufvar(a:bufnr, 'ale_linters', [])
  call remove(lint_list, index(lint_list, 'nasm'))
endfunction
