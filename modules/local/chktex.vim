" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Check LaTeX documents with chktex

" Mod info
let g:Module_chktex_type = 'local'
let g:Module_chktex_priority = 3
let g:Module_chktex_filetypes = ['tex']
let g:Module_chktex_requires = ['ale']

function! g:Module_chktex_load(bufnr)
  call setbufvar(a:bufnr, 'ale_linters', getbufvar(a:bufnr, 'ale_linters', []) + ['chktex'])
endfunction

function! g:Module_chktex_unload(bufnr)
  let lint_list = getbufvar(a:bufnr, 'ale_linters', [])
  call remove(lint_list, index(lint_list, 'chktex'))
endfunction
