" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Configuration common for all documentation formats

" Mod info
let g:Module_common_docs_type = 'local'
let g:Module_common_docs_priority = 4
let g:Module_common_docs_filetypes = ['markdown', 'tex', 'asciidoc']
let g:Module_common_docs_requires = ['pencil', 'vim_ditto', 'textobj_quote', 'textobj_sentence']

function! g:Module_common_docs_load(bufnr)
  " Initialize vim-pencil
  call pencil#init({'wrap': 'hard', 'autoformat': 0})

  " Spell
  call setbufvar(a:bufnr, '&spell', 1)
  call setbufvar(a:bufnr, '&spelllang', 'en_gb')
endfunction

function! g:Module_common_docs_unload(bufnr)
  " Disable vim-pencil
  call pencil#init({'wrap': 'off'})

  " Spell
  call setbufvar(a:bufnr, '&spell', &g:spell)
  call setbufvar(a:bufnr, '&spelllang', &g:spelllang)
endfunction
