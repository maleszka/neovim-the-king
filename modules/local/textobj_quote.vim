" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Support typographic quotes

" Mod info
let g:Module_textobj_quote_type = 'local'
let g:Module_textobj_quote_priority = 4
let g:Module_textobj_quote_filetypes = ['asciidoc', 'markdown', 'tex']
let g:Module_textobj_quote_requires = ['plug_list']

" Configuration variable
let g:Module_textobj_quote_conftable = {'double':'“”', 'single':'‘’'}

function! g:Module_textobj_quote_conf(bufnr)
  let splang = split(getbufvar(a:bufnr, '&spelllang', 'en'), ',')[0]

  if splang == 'en'
    let g:Module_textobj_quote_conftable = {'double':'“”', 'single':'‘’'}
  elseif splang == 'de'
    let g:Module_textobj_quote_conftable = {'double': '„“', 'single':'‚’'}
  elseif splang == 'pl'
    let g:Module_textobj_quote_conftable = {'double': '„“', 'single':'«»'}
  endif

  call textobj#quote#init(g:Module_textobj_quote_conftable)
endfunction

function! g:Module_textobj_quote_load(bufnr)
  " ==== Load ====
  call plug#load(['vim-textobj-user', 'vim-textobj-quote'])

  " ==== Configure ====
  let g:textobj#quote#educate = 1
  let g:textobj#quote#matchit = 1

  " ==== Initialize ====
  call g:Module_textobj_quote_conf(a:bufnr)

  " QuoteConf - use it after changing spelllang option
  command! -buffer QuoteConf call g:Module_textobj_quote_conf(1)
endfunction

function! g:Module_textobj_quote_unload(bufnr)
  NoEducate
  NoMatchParen
endfunction
