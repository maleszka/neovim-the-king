" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Enables additional syntax highlighting using lsp info

" Mod info
let g:Module_lsp_cxx_hl_type = 'local'
let g:Module_lsp_cxx_hl_priority = 3
let g:Module_lsp_cxx_hl_filetypes = ['cpp']
let g:Module_lsp_cxx_hl_requires = ['vim_lsp_cxx_highlight', 'lspconfig']

function! g:Module_lsp_cxx_hl_load(bufnr)
  LspCxxHighlight
endfunction

function! g:Module_lsp_cxx_hl_unload(bufnr)
  LspCxxHighlightDisable
endfunction
