" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Tell vim-plug which plugins might be used

" Mod info
let g:Module_plug_list_type = "global"
let g:Module_plug_list_priority = 0
let g:Module_plug_list_requires = ['install_vim_plug']

function! g:Module_plug_list_load()
  call plug#begin(expand('~/.vim/plugged'))

  " ==== ==== Behaviour ==== ====

  " ==== Typing ====
  " auto-pairs - brackets pairing
  Plug 'LunarWatcher/auto-pairs', {'on': 'Demand'}

  " ultisnips - snippets engine
  Plug 'SirVer/ultisnips', {'on': 'Demand'}

  " vim-snippets - set of Ultisnip snippets
  Plug 'honza/vim-snippets', {'on': 'Demand'}

  " completion-nvim - completion for built-in LSP.
  Plug 'nvim-lua/completion-nvim', {'on': 'Demand'}
  Plug 'steelsojka/completion-buffers', {'on': 'Demand'}

  " vim-pencil - bundle of useful functions for writing
  Plug 'preservim/vim-pencil', {'on': 'Demand'}

  " vim-textobj-user - system for creating text objects
  Plug 'kana/vim-textobj-user', {'on': 'Demand'}

  " vim-textobj-quote - support for typographic quotes
  Plug 'preservim/vim-textobj-quote', {'on': 'Demand'}

  " vim-textobj-sentence - improved sentence text objects
  Plug 'preservim/vim-textobj-sentence', {'on': 'Demand'}

  " ==== Formatting ====
  " kommentary - lightweight lua plugin to comment in/out text
  Plug 'b3nj5m1n/kommentary', {'on': 'Demand'}

  " ==== Checking ====
  " vim-dialect - project-specific spellfiles
  Plug 'dbmrq/vim-dialect', {'on': 'Demand'}

  " vim-ditto - highlight overused words
  Plug 'dbmrq/vim-ditto', {'on': 'Demand'}

  " ale - lint engine that uses LSP.
  Plug 'dense-analysis/ale', {'on': 'Demand'}

  " nvim-lsp-ale - bridge between nvim-lsp and ALE
  Plug g:config_home . 'plugged/nvim-lsp-ale', {'on': 'Demand'}

  " nvim-lspconfig - collection of nvim-lsp configurations
  Plug 'neovim/nvim-lspconfig', {'on': 'Demand'}

  " ==== Layout ====
  " nvim-toggleterm.lua - toggle terminal written in lua
  Plug 'akinsho/nvim-toggleterm.lua', {'on': 'Demand'}

  " undotree - history tree drawer
  Plug 'mbbill/undotree', {'on': 'Demand'}

  " nvim-gdb - wrapper for GDB, LLDB, PDB, and BashDB
  Plug 'sakhnik/nvim-gdb', {'on': 'Demand'}

  " hexmode - edit binary files with help of the xxd
  Plug 'fidian/hexmode', {'on': 'Hexmode'}

  " vim-windowswap - swap windows without ruining your layout
  Plug 'wesQ3/vim-windowswap', {'on': 'Demand'}

  " lsp-status - LSP progress on statusline.
  Plug 'nvim-lua/lsp-status.nvim', {'on': 'Demand'}

  " ==== Folds ====
  " nvim-lsp-fold - use LSP as folds' source
  Plug g:config_home . 'plugged/nvim-lsp-fold', {'on': 'Demand'}

  " ==== Searching ====
  " fzf - fuzzy finder via fzf
  Plug '/opt/fzf', {'on': 'Demand'}
  Plug 'junegunn/fzf.vim', {'on': 'Demand'}

  " fzf-lsp-nvim - fuzzy finder for lsp symbols
  Plug 'gfanto/fzf-lsp.nvim', {'on': 'Demand'}

  " ==== Misc ====
  " vim-fugitive - git support for Neovim
  Plug 'tpope/vim-fugitive', {'on': 'Demand'}

  " nvim-miniyank - yank across nvim instances
  Plug 'bfredl/nvim-miniyank', {'on': 'Demand'}

  " markdown-preview - preview markdown files in the browser
  Plug 'iamcco/markdown-preview.nvim', {'do': 'cd app && yarn install', 'on': 'Demand'}

  " cmake4vim - integration for CMake build system generator
  Plug 'ilyachur/cmake4vim', {'on': 'Demand'}

  " ==== ==== Appearance ==== ====

  " ==== Colors ====
  " ayu-vim - elegant dark theme for vim
  Plug 'ayu-theme/ayu-vim', {'on': 'Demand'}

  " ==== Syntax ====
  " vim-lsp-cxx-highlight - better cpp highlighting using LSP
  Plug 'jackguo380/vim-lsp-cxx-highlight', {'on': 'Demand'}

  " ==== Layout ====
  " vim-airline - status bar
  Plug 'vim-airline/vim-airline', {'on': 'Demand'}
  Plug 'vim-airline/vim-airline-themes', {'on': 'Demand'}

  " vim-gitgutter - git diff sign column.
  Plug 'airblade/vim-gitgutter', {'on': 'Demand'}

  call plug#end()
endfunction
