" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Enable ayu colorscheme

" Mod info
let g:Module_common_settings_type = 'global'
let g:Module_common_settings_priority = 5
let g:Module_common_settings_requires = []

function! g:Module_common_settings_load()
  " ==== ==== ==== General ==== ==== ====

  " ==== ==== Editor ==== ====
  " Ward off unexpected things that your distribution might have made.
  set nocompatible

  " ==== ==== Buffers ==== ====
  " Allow hiding buffers.
  set hidden

  " ==== ==== Filetype ==== ====
  " Enable filetype detection
  filetype plugin indent on

  " ==== ==== ==== Behaviour ==== ==== ====

  " ==== ==== Typing ==== ====
  " Allow backspacing over auto-indent
  set backspace=indent,eol,start

  " ==== Completion ====
  set completeopt=menuone,noselect
  set shortmess+=c

  " ==== ==== Undo ==== ====
  " Save undo history to file
  set undofile
  exec "set undodir=" . g:config_home . ".undo"

  " ==== ==== Indenting ==== ====
  " When opening a new line and no filetype-specific indenting is enabled, keep
  " the same indent as the line you're currently on. Useful for READMEs, etc.
  set autoindent

  " Make indent consist of two spaces
  set shiftwidth=2
  set softtabstop=2
  set expandtab
  set tabstop=2

  " ==== ==== Layout ==== ====
  " Horizontal split will now create new window below/right previous
  set splitbelow
  set splitright

  " ==== ==== Searching ==== ====
  " Highlight searches (:nohl to clean highlight )
  set hlsearch
  set incsearch

  " Use case insensitive search, except when using capital letters
  set ignorecase
  set smartcase

  " ==== ==== Ex mode ==== ====
  " Enable better ex-mode completion.
  set wildmenu

  " Instead of failing a command because of unsaved changes, instead raise a
  " dialogue asking if you wish to save changed files.
  set confirm

  " Set the command window height.
  set cmdheight=2

  " Quickly time out on keycodes, but never time out on mappings.
  set notimeout ttimeout ttimeoutlen=200

  " ==== ==== Folds ==== ====
  set foldopen=block,hor,jump,mark,percent,quickfix,search,tag,undo

  " ==== ==== Misc ==== ====
  " Stop certain movements from always going to the first character of a line.  While this behavior deviates from that of Vi, it does what most users
  " coming from other editors would expect.
  set nostartofline


  " ==== ==== ==== Appearance ===== =====

  " ==== ==== Colors ==== ====
  set background=dark

  " Enable true color
  set termguicolors

  " Set ColorColumn color
  highlight ColorColumn ctermbg=darkgray

  " ==== terminal ====
  let g:terminal_color_1 = "#c46140"
  let g:terminal_color_2 = "#60c440"
  let g:terminal_color_3 = "#c4c440"
  let g:terminal_color_4 = "#4060c4"
  let g:terminal_color_5 = "#c440c4"
  let g:terminal_color_6 = "#40c4c4"

  " ==== ==== Interface ==== ====
  " Display the cursor position on the last line of the screen or in the status
  " line of a window.
  set ruler

  " Do not show current keystroke in left-bottom corner
  set noshowcmd

  " Always display the status line, even if only one window is displayed.
  set laststatus=2

  " Display line numbers on the left.
  set number
  set numberwidth=5

  " Make updates more frequent
  set updatetime=500

  " Display signs in number column
  set signcolumn=number

  " ==== ==== Scrolling ====
  " Always show at least one line above/below the cursor.
  set scrolloff=2

  " ==== ==== Syntax ==== ====
  " Enable syntax highlighting.
  syntax on

  " ==== ==== Cursor ==== ====
  " Highlight cursor line
  set cursorline

  " Every time Vim leaves, it restores shape to the shell's defaults
  augroup RestoreCursorShapeOnExit
    autocmd!
    autocmd VimLeave * set guicursor=a:ver25-blinkwait200-blinkoff300-blinkon400
  augroup END

endfunction
