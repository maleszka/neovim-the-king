" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Configuration for editing vimL

" Mod info
let g:Module_vim_type = 'local'
let g:Module_vim_priority = 3
let g:Module_vim_filetypes = ['vim']
let g:Module_vim_requires = ['lspconfig', 'nvim_lsp_fold', 'fzf_lsp']

function! g:Module_vim_load(bufnr)
  " Text width
  let &l:textwidth = 74
  let &l:colorcolumn = 74

  " Spell
  let &l:spell = 1
  let &l:spelllang = 'en'

  " Start lsp clients
  LspStart
endfunction

function! g:Module_vim_unload(bufnr)
  " Text width
  let &l:textwidth = &g:textwidth
  let &l:colorcolumn = &g:colorcolumn

  " Spell
  let &l:spell = &g:spell
  let &l:spelllang = &g:spelllang

  " Stop lsp clients
  LspStop
endfunction
