" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Additional configuration for nvimgdb buffers

" Mod info
let g:Module_nvimgdb_type = 'local'
let g:Module_nvimgdb_priority = 5
let g:Module_nvimgdb_filetypes = ['nvimgdb']
let g:Module_nvimgdb_requires = []

function! g:Module_nvimgdb_load(bufnr)
  " Disable numbers
  call setbufvar(a:bufnr, '&number', 0)
endfunction

function! g:Module_nvimgdb_unload(bufnr)
  " Reset number
  call setbufvar(a:bufnr, '&number', &g:number) 
endfunction
