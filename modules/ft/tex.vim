" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Set up editor for LaTeX

" Mod info
let g:Module_tex_type = 'local'
let g:Module_tex_priority = 4
let g:Module_tex_filetypes = ['tex']
let g:Module_tex_requires = ['lspconfig', 'nvim_lsp_fold', 'fzf_lsp', 'common_docs']

function! g:Module_tex_load(bufnr)
  " Start lsp clients
  LspStart
endfunction

function! g:Module_tex_unload(bufnr)
  " Stop lsp clients
  LspStop
endfunction
