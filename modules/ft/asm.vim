" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Map assembly filetype to nasm flavour

" Mod info
let g:Module_asm_type = 'local'
let g:Module_asm_priority = 5
let g:Module_asm_filetypes = ['asm']
let g:Module_asm_requires = []

function! g:Module_asm_load(bufnr)
  " Set filetype to nasm
  call setbufvar(a:bufnr, '&filetype', 'nasm')
endfunction

function! g:Module_asm_unload(bufnr)
  " Set filetype to asm
  call setbufvar(a:bufnr, '&filetype', 'asm')
endfunction
