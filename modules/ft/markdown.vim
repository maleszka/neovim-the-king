" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Markdown-specific configuration

" Mod info
let g:Module_markdown_type = 'local'
let g:Module_markdown_priority = 4
let g:Module_markdown_filetypes = ['markdown']
let g:Module_markdown_requires = ['common_docs', 'markdown_preview']

function! g:Module_markdown_load(bufnr)
endfunction

function! g:Module_markdown_unload(bufnr)
endfunction
