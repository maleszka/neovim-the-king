" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Set of useful features for CMake files

" Mod info
let g:Module_cmake_type = 'local'
let g:Module_cmake_priority = 3
let g:Module_cmake_filetypes = ['cmake']
let g:Module_cmake_requires = ['lspconfig']

function! g:Module_cmake_load(bufnr)
  " Text width
  call setbufvar(a:bufnr, '&textwidth', 74)
  call setbufvar(a:bufnr, '&colorcolumn', 74)

  " Spell
  call setbufvar(a:bufnr, '&spell', 1)
  call setbufvar(a:bufnr, '&spelllang', 'en')

  " Start lsp clients
  LspStart

  " Format on save (optional)
  " augroup nvim-lsp-format
  "   autocmd!
  "   autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_seq_sync()
  " augroup END
endfunction

function! g:Module_cmake_unload(bufnr)
  " Spell
  call setbufvar(a:bufnr, '&spell', &g:spell)
  call setbufvar(a:bufnr, '&spelllang', &g:spelllang)

  " Stop lsp clients
  LspStop
endfunction
