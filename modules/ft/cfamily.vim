" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Set of useful features for C-family languages

" Mod info
let g:Module_cfamily_type = 'local'
let g:Module_cfamily_priority = 4
let g:Module_cfamily_filetypes = ['cpp']
let g:Module_cfamily_requires = ['lspconfig', 'nvim_lsp_fold', 'fzf_lsp', 'nvim_gdb']

function! g:Module_cfamily_load(bufnr)
  " Text width
  let &l:textwidth = 74
  let &l:colorcolumn = 74

  " Spell
  let &l:spell = 1
  let &l:spelllang = 'en'

  " Start lsp clients
  LspStart

  " Format on save
  augroup nvim-lsp-format
    autocmd!
    autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_seq_sync()
  augroup END
endfunction

function! g:Module_cfamily_unload(bufnr)
  " Text width
  let &l:textwidth = &g:textwidth
  let &l:colorcolumn = &g:colorcolumn

  " Spell
  let &l:spell = &g:spell
  let &l:spelllang = &g:spelllang

  " Stop lsp clients
  LspStop
endfunction
