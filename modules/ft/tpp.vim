" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Treat tpp files as cpp files

" Mod info
let g:Module_tpp_type = 'local'
let g:Module_tpp_priority = 5
let g:Module_tpp_filetypes = ['tpp']
let g:Module_tpp_requires = []

function! g:Module_tpp_load(bufnr)
  " Set filetype to cpp
  call setbufvar(a:bufnr, '&filetype', 'cpp')
endfunction

function! g:Module_tpp_unload(bufnr)
  " Set filetype to tpp
  call setbufvar(a:bufnr, '&filetype', 'tpp')
endfunction
