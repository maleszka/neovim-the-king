" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Install vim-plug minimalistic plugin manager

" Mod info
let g:Module_install_vim_plug_type = "global"
let g:Module_install_vim_plug_priority = 0

function! g:Module_install_vim_plug_load()
  let s:plug_vim_path = expand('~/.local/share/nvim/site/autoload/plug.vim')

  if !filereadable(s:plug_vim_path)
    echo "Installing vim-plug ..."
    execute "!curl -fLo " .. s:plug_vim_path .. " --create-dirs https://gitlab.com/maleszka/vim-plug/-/raw/master/plug.vim"
    echo "Vim-plug installed successfully"
  end
endfunction
