" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Use project-specific spellfiles

" Mod info
let g:Module_vim_dialect_type = 'global'
let g:Module_vim_dialect_priority = 5
let g:Module_vim_dialect_requires = ['plug_list']

function! g:Module_vim_dialect_load()
  call plug#load('vim-dialect')
endfunction
