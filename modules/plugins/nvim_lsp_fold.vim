" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Folds' LSP source

" Mod info
let g:Module_nvim_lsp_fold_type = 'global'
let g:Module_nvim_lsp_fold_priority = 0
let g:Module_nvim_lsp_fold_requires = ['plug_list', 'lspconfig']

function! g:Module_nvim_lsp_fold_load()
  " Config
  let g:nvim_lsp_fold#update_on_save = v:false

  " Load
  call plug#load('nvim-lsp-fold')

  " Commands
  command! Fold lua require'nvim-lsp-fold'.update_folds()
endfunction
