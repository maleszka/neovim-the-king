" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Provide more highlighting using nvim lsp info

" Mod info
let g:Module_vim_lsp_cxx_highlight_type = 'global'
let g:Module_vim_lsp_cxx_highlight_priority = 0
let g:Module_vim_lsp_cxx_highlight_requires = ['plug_list', 'lspconfig']

function! g:Module_vim_lsp_cxx_highlight_load()
  " Load
  call plug#load('vim-lsp-cxx-highlight')
endfunction
