" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Configuration of cmake4vim plugin

" Mod info
let g:Module_cmake4vim_type = 'global'
let g:Module_cmake4vim_priority = 3
let g:Module_cmake4vim_requires = ['plug_list']

" Utility functions
function s:match(dir, pattern)
  if a:pattern[0] == '='
    return s:is(a:dir, a:pattern[1:])
  elseif a:pattern[0] == '^'
    return s:sub(a:dir, a:pattern[1:])
  elseif a:pattern[0] == '>'
    return s:child(a:dir, a:pattern[1:])
  else
    return s:has(a:dir, a:pattern)
  endif
endfunction


" Returns true if dir is identifier, false otherwise.
" dir        - full path to a directory
" identifier - a directory name
function! s:is(dir, identifier)
  let identifier = substitute(a:identifier, '/$', '', '')
  return fnamemodify(a:dir, ':t') ==# identifier
endfunction


" Returns true if dir contains identifier, false otherwise.
" dir        - full path to a directory
" identifier - a file name or a directory name; may be a glob
function! s:has(dir, identifier)
  " We do not want a:dir to be treated as a glob so escape any wildcards.
  " If this approach is problematic (e.g. on Windows), an alternative
  " might be to change directory to a:dir, call globpath() with just
  " a:identifier, then restore the working directory.
  return !empty(globpath(escape(a:dir, '?*[]'), a:identifier, 1))
endfunction


" Returns true if identifier is an ancestor of dir,
" i.e. dir is a subdirectory (no matter how many levels) of identifier;
" false otherwise.
" dir        - full path to a directory
" identifier - a directory name
function! s:sub(dir, identifier)
  let path = s:parent(a:dir)
  while 1
    if fnamemodify(path, ':t') ==# a:identifier | return 1 | endif
    let [current, path] = [path, s:parent(path)]
    if current == path | break | endif
  endwhile
  return 0
endfunction

" Return true if identifier is a direct ancestor (parent) of dir,
" i.e. dir is a direct subdirectory (child) of identifier; false otherwise
"
" dir        - full path to a directory
" identifier - a directory name
function! s:child(dir, identifier)
  let path = s:parent(a:dir)
  return fnamemodify(path, ':t') ==# a:identifier
endfunction

" Returns full path of directory of current file name (which may be a directory).
function! s:current()
  let fn = expand('%:p', 1)
  if fn =~ 'NERD_tree_\d\+$' | let fn = b:NERDTree.root.path.str().'/' | endif
  if empty(fn) | return getcwd() | endif  " opening vim without a file
  return fnamemodify(fn, ':h')
endfunction

" Returns full path of dir's parent directory.
function! s:parent(dir)
  return fnamemodify(a:dir, ':h')
endfunction

" Returns a parent containing specified patterns (project root)
function s:root_pattern(patterns)
  let dir = s:current()

  " breadth-first search
  while 1
    for pattern in a:patterns
      if pattern[0] == '!'
        let [p, exclude] = [pattern[1:], 1]
      else
        let [p, exclude] = [pattern, 0]
      endif
      if s:match(dir, p)
        if exclude
          break
        else
          return dir
        endif
      endif
    endfor

    let [current, dir] = [dir, s:parent(dir)]
    if current == dir | break | endif
  endwhile

  return ''
endfunction

function! g:Module_cmake4vim_load()
  " ==== Configuration ====
  " Building options
  let g:cmake_compile_commands = 0
  let g:make_arguments = '-j'

  " Directories
  let g:cmake_src_dir = s:root_pattern(['.git', 'README.md', '.dialectmain'])
  let g:cmake_build_dir_prefix = g:cmake_src_dir . "/build-"

  " ==== Keybindings ====
  nnoremap <silent> <leader>cm :CMakeResetAndReload<CR>
  nnoremap <silent> <leader>cb :CMakeBuild<CR>
  nnoremap <silent> <leader>cr :CMakeReset<CR>
  nnoremap <silent> <leader>ct :FZFCMakeSelectTarget<CR>

  " ==== Load plugin ====
  call plug#load('cmake4vim')
endfunction
