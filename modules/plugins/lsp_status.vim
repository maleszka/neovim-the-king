" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Set of utilities used to get status of built-in lsp, here used
" to display ls progress on airline

" Mod info
let g:Module_lsp_status_type = 'global'
let g:Module_lsp_status_priority = 2
let g:Module_lsp_status_requires = ['plug_list', 'vim_airline', 'lspconfig']

function! g:Module_lsp_status_load()
  " Load plugin
  call plug#load('lsp-status.nvim')

lua << EOF
local lsp_status = require('lsp-status')

lsp_status.register_progress()

-- Capabilities
if _G.capabilities == nil then _G.capabilities = vim.lsp.protocol.make_client_capabilities() end
_G.capabilities.window = _G.capabilities.window or {}
_G.capabilities.window.workDoneProgress = true

EOF

  " Helper function that returns lsp status
  function! LspStatus() abort
    let status = luaeval('require("lsp-status/statusline").progress()')
    return trim(substitute(status, '%%', '%', ''))
  endfunction

  " Add lsp progress to airline
  call airline#parts#define_function('lsp_status', 'LspStatus')
  call airline#parts#define_condition('lsp_status', 'luaeval("#vim.lsp.buf_get_clients() > 0")')
  let g:airline_section_c = airline#section#create(['%<', 'file', g:airline_symbols.space, 'readonly', 'lsp_status'])
endfunction
