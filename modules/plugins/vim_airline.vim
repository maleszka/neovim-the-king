" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Good-looking advanced status line

" Mod info
let g:Module_vim_airline_type = 'global'
let g:Module_vim_airline_priority = 2
let g:Module_vim_airline_requires = ['plug_list']

function! g:Module_vim_airline_load()
  " Configuration
  let g:airline#extensions#tabline#enabled = 1
  let g:airline_theme="zenburn"
  let g:airline#extensions#tabline#show_buffers = 0
  let g:airline#extensions#tabline#show_tabs = 1
  let g:airline#extensions#tabline#buffers_label = 'b'
  let g:airline#extensions#tabline#tabs_label = 't'
  let g:airline#extensions#tabline#fnamemod = ':t'
  let g:airline#extensions#tabline#tab_min_count = 2
  let g:airline#extensions#tabline#show_close_button = 0

  " Disable loading rtp automatically
  let g:airline#extensions#disable_rtp_load = 1

  " Disable some extensions
  let g:airline#extensions#nvimlsp#enabled = 0
  let g:airline#extensions#po#enabled = 0

  " Loading
  call plug#load('vim-airline-themes', 'vim-airline')
endfunction
