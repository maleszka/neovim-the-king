" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: GNU debugger support (nvim-gdb plugin)

" Mod info
let g:Module_nvim_gdb_type = 'global'
let g:Module_nvim_gdb_priority = 0
let g:Module_nvim_gdb_requires = ['plug_list']

function! g:Module_nvim_gdb_load()
  " Configuration
  let g:nvimgdb_config_override = {
    \ 'sign_current_line': '>',
    \ 'sign_breakpoint': ['*', '#'],
    \ 'codewin_command': 'topleft new'
    \ }
  
  " Load
  call plug#load('nvim-gdb')
endfunction
