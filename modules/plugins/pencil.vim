" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Bundle of useful functions for writing

" Mod info
let g:Module_pencil_type = 'global'
let g:Module_pencil_priority = 0
let g:Module_pencil_requires = ['plug_list']

function! g:Module_pencil_load()
  " ==== Configuration ====
  " Disable conceal feature.
  let g:pencil#conceallevel = 0
  let g:pencil#concealcursor = "" 

  " Disable auto-format feature.
  let g:pencil#autoformat = 0

  " ==== Load ====
  call plug#load('vim-pencil')
endfunction
