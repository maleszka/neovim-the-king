" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Quickly open files/buffers etc. using fzf

" Mod info
let g:Module_fzf_type = 'global'
let g:Module_fzf_priority = 4
let g:Module_fzf_requires = ['plug_list']

function! g:Module_fzf_load()
  " Configuration
  let g:fzf_layout = {'down': '12'}
  let g:fzf_preview_window = ['right:50%:hidden', 'ctrl-/']

  " Loading
  call plug#load('fzf', 'fzf.vim')

  " Mappings
  nnoremap <silent> <leader>ff :Files<CR>
  nnoremap <silent> <leader>fb :Buffers<CR>
  nnoremap <silent> <leader>fl :BLines<CR>
  nnoremap <silent> <leader>fh :Helptags<CR>
endfunction
