" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Share Neovim yank registers across Neovim instances

" Mod info
let g:Module_miniyank_type = 'global'
let g:Module_miniyank_priority = 4
let g:Module_miniyank_requires = ['plug_list']

function! g:Module_miniyank_load()
  " Load
  call plug#load('nvim-miniyank')

  " Mappings
  nmap p <Plug>(miniyank-autoput)
  nmap P <Plug>(miniyank-autoPut)

  nmap <leader>yp <Plug>(miniyank-startput)
  nmap <leader>yP <Plug>(miniyank-startPut)

  nmap <leader>yn <Plug>(miniyank-cycle)
  nmap <leader>yN <Plug>(miniyank-cycleback)

  nmap <leader>yc <Plug>(miniyank-tochar)
  nmap <leader>yl <Plug>(miniyank-toline)
  nmap <leader>yb <Plug>(miniyank-toblock)
endfunction
