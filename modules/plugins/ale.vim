" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Load

" Mod info
let g:Module_ale_type = 'global'
let g:Module_ale_priority = 0
let g:Module_ale_requires = ['plug_list']

function! g:Module_ale_load()
  " ==== Configuration ====
  " Don't use linters that wasn't enabled explicitly.
  let g:ale_linters_explicit = 1

  " Run linters less frequently.
  let g:ale_lint_on_text_changed = "never"
  let g:ale_lint_on_insert_leave = 0

  " Method of showing errors.
  let g:ale_echo_msg_format = '%severity% (%linter%): %code: %%s'

  " ==== Loading ====
  call plug#load('ale')
endfunction
