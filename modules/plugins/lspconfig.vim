" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Set of language servers' configurations

" Mod info
let g:Module_lspconfig_type = 'global'
let g:Module_lspconfig_priority = 0
let g:Module_lspconfig_requires = ['plug_list', 'nvim_lsp_ale']

function! g:Module_lspconfig_load()
  " Load nvim-lspconfig plugin
  call plug#load('nvim-lspconfig')

lua << EOF
-- ==== ==== Variables ==== ====
local lspconfig = require('lspconfig')

-- Make sure _G.capabilities exists
if _G.capabilities == nil then _G.capabilities = vim.lsp.protocol.make_client_capabilities() end

-- ==== ==== Utils ==== ====
-- Set buffer-local keymap
local function bmap(t, key, value)
vim.api.nvim_buf_set_keymap(0, t, key, value, {noremap = true, silent = true})
end

-- Returns true if lua module exists and can be loaded
local function lua_mod_exists(name)
  if package.loaded[name] then
    return true
  else
    for _, searcher in ipairs(package.searchers or package.loaders) do
      local loader = searcher(name)
      if type(loader) == "function" then
        return true
      end
    end
    return false
  end
end

-- ==== ==== Custom attach ==== ====
local function custom_attach(client, bufnr)
  -- nvim_lsp_fold
  if lua_mod_exists('nvim-lsp-fold') then
    require('nvim-lsp-fold').on_attach(client, bufnr)
  end

  -- lsp_status
  if lua_mod_exists('lsp-status') then
    require('lsp-status').on_attach(client)
  end

  -- completion
  if lua_mod_exists('completion') then
    require('completion').on_attach()
  end

  -- Keybindings
  bmap('n', 'af', '<cmd>lua vim.lsp.buf.code_action()<CR>')
  bmap('n', 'gD', '<cmd> lua vim.lsp.buf.declaration()<CR>')
  bmap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>')
  bmap('n', '<leader>gw', '<cmd>lua vim.lsp.buf.document_symbol()<CR>')
  bmap('n', '<leader>gW', '<cmd>lua vim.lsp.buf.workspace_symbol()<CR>')
  bmap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>')
  bmap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>')
  bmap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>')
  bmap('n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>')
  bmap('n', 'gt', '<cmd>lua vim.lsp.buf.type_definition()<CR>')
end

-- ==== ==== Language servers ==== ====
-- ==== bashls ====
lspconfig.bashls.setup {
  on_attach = custom_attach,
  capabilities = _G.capabilities
}

-- ==== ccls ====
lspconfig.ccls.setup({
  cmd = { 'ccls', '--log-file=/tmp/ccls.log', '-v=1' },
  init_options = {
    cache = {
      directory = '.ccls-cache',
    };
    client = {
      snippetSupport = true,
    };
    diagnostics = {
      onChange = 1000,
    };
    highlight = {
      lsRanges = true,
    };
    index = {
      onChange = true,
    };
  },
  root_dir = function(fname)
    return lspconfig.util.root_pattern('.ccls', 'compile_commands.json', 'compile_flags.json', '.git')(fname) or lspconfig.util.path.dirname(fname)
  end,
  on_attach = custom_attach,
  capabilities = _G.capabilities
})

-- ==== cmake ====
lspconfig.cmake.setup {
  on_attach = custom_attach,
  capabilities = _G.capabilities
}

-- ==== texlab ===
lspconfig.texlab.setup {
  settings = {
    texlab = {
      build = {
        executable = 'latexmk',
        args = {'-xelatex', '-interaction=nonstopmode', '-synctex=1', '%f'},
        onSave = true,
      },
      forwardSearch = {
        executable = 'zathura',
        args = {"--synctex-forward", "%l:1:%f", "%p"}
      },
      diagnosticsDelay = 1000,
    }
  },
  on_attach = custom_attach,
  capabilities = _G.capabilities
}

-- ==== vimls ====
lspconfig.vimls.setup({
  on_attach = custom_attach,
  capabilities = _G.capabilities
})
EOF
endfunction
