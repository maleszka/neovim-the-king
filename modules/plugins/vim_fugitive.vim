" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Git support for Neovim

" Mod info
let g:Module_vim_fugitive_type = 'global'
let g:Module_vim_fugitive_priority = 3
let g:Module_vim_fugitive_requires = ['plug_list']

function! g:Module_vim_fugitive_load()
  call plug#load('vim-fugitive')
endfunction
