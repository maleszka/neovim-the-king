" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Send LSP diagnostics to ALE

" Mod info
let g:Module_nvim_lsp_ale_type = 'global'
let g:Module_nvim_lsp_ale_priority = 0
let g:Module_nvim_lsp_ale_requires = ['plug_list', 'ale']

function! g:Module_nvim_lsp_ale_load()
  " Load
  call plug#load('nvim-lsp-ale')
endfunction
