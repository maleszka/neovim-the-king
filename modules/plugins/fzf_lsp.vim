" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Search code information provided by LSP using fzf

" Mod info
let g:Module_fzf_lsp_type = 'global'
let g:Module_fzf_lsp_priority = 0
let g:Module_fzf_lsp_requires = ['plug_list', 'lspconfig', 'fzf']

function! g:Module_fzf_lsp_load()
  " Load
  call plug#load('fzf-lsp.nvim')

  " Mappings
  nnoremap <silent> <leader>fgd :Definitions<CR>
  nnoremap <silent> <leader>fgr :References<CR>
  nnoremap <silent> <leader>fw :DocumentSymbols<CR>
  nnoremap <silent> <leader>fW :WorkspaceSymbols<CR>
endfunction
