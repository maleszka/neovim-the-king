" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Quickly comment lots of lines

" Mod info
let g:Module_kommentary_type = 'global'
let g:Module_kommentary_priority = 4
let g:Module_kommentary_requires = ['plug_list']

function! g:Module_kommentary_load()
  " Load
  call plug#load('kommentary')

  " Configure
lua << EOF
  require'kommentary.config'.configure_language("default", {
    prefer_single_line_comments = true,
    use_consistent_indentation = true,
    ignore_whitespace = true
  })
EOF
endfunction
