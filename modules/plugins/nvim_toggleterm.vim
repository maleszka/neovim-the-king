" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Simple toggle terminal

" Mod info
let g:Module_nvim_toggleterm_type = 'global'
let g:Module_nvim_toggleterm_priority = 4
let g:Module_nvim_toggleterm_requires = ['plug_list']

function! g:Module_nvim_toggleterm_load()
  call plug#load('nvim-toggleterm.lua')

lua << EOF
  require('toggleterm').setup({
    size = 12,
    open_mapping = [[<c-t>]],
    hide_numbers = true,
    shade_terminals = true,
    shading_factor = 0.5,
    start_in_insert = true,
    direction = 'horizontal',
    close_on_exit = true
  })
EOF

endfunction
