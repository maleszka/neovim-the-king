" Mod info
let g:Module_ultisnips_type = 'global'
let g:Module_ultisnips_priority = 3
let g:Module_ultisnips_requires = ['plug_list']

function! g:Module_ultisnips_load()
  " ==== Configuration ====
  let g:UltiSnipsEditSplit = 'vertical'

  " ==== Loading ====
  call plug#load('ultisnips', 'vim-snippets')
endfunction
