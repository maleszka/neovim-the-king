" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Showing changed lines, staging them etc.

" Mod info
let g:Module_gitgutter_type = 'global'
let g:Module_gitgutter_priority = 2
let g:Module_gitgutter_requires = ['plug_list']

function! g:Module_gitgutter_load()
  " Decrease the sign piority to show breakpoints while debugging.
  let g:gitgutter_sign_piority = 5

  " Disable signs, they use a lot of processing power
  let g:gitgutter_signs = v:false

  " Close preview window using <Esc>
  let g:gitgutter_close_preview_on_escape = v:true

  " Load
  call plug#load('vim-gitgutter')
endfunction
