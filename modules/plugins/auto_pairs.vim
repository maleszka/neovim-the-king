" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Pair brackets automatically

" Mod info
let g:Module_auto_pairs_type = 'global'
let g:Module_auto_pairs_priority = 3
let g:Module_auto_pairs_requires = ['plug_list']

function! g:Module_auto_pairs_load()
  " Configuration
  let g:AutoPairsFlyMode = 0

  let g:AutoPairsCompatibleMaps = 0

  let g:AutoPairsMapBS = 1
  let g:AutoPairsMultilineBackspace = 1

  " Loading
  call plug#load('auto-pairs')
endfunction
