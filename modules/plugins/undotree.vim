" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Undo tree drawer

" Mod info
let g:Module_undotree_type = 'global'
let g:Module_undotree_priority = 2
let g:Module_undotree_requires = ['plug_list']

function! g:Module_undotree_load()
  " ==== Configuration ====
  " Undotree toggle
  nnoremap <silent> <C-w><C-u> :UndotreeToggle<CR>

  " Set Undotree layout style to 1 (left)
  let g:undotree_WindowLayout = 1

  " Using 'd' instead of 'days' to save some space
  if !exists('g:undotree_ShortIndicators')
    let g:undotree_ShortIndicators = 0
  endif

  " undotree window width
  if !exists('g:undotree_SplitWidth')
      if g:undotree_ShortIndicators == 1
          let g:undotree_SplitWidth = 24
      else
          let g:undotree_SplitWidth = 30
      endif
  endif

  " diff window height
  if !exists('g:undotree_DiffpanelHeight')
      let g:undotree_DiffpanelHeight = 10
  endif

  " if set, let undotree window get focus after being opened, otherwise
  " focus will stay in current window.
  if !exists('g:undotree_SetFocusWhenToggle')
      let g:undotree_SetFocusWhenToggle = 1
  endif

  " tree node shape.
  if !exists('g:undotree_TreeNodeShape')
      let g:undotree_TreeNodeShape = '*'
  endif

  if !exists('g:undotree_DiffCommand')
      let g:undotree_DiffCommand = "diff"
  endif

  " relative timestamp
  if !exists('g:undotree_RelativeTimestamp')
      let g:undotree_RelativeTimestamp = 1
  endif

  " Highlight changed text
  if !exists('g:undotree_HighlightChangedText')
      let g:undotree_HighlightChangedText = 1
  endif

  " Highlight changed text using signs in the gutter
  if !exists('g:undotree_HighlightChangedWithSign')
      let g:undotree_HighlightChangedWithSign = 1
  endif

  " ==== Loading ====
  call plug#load('undotree')
endfunction
