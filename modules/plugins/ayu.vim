" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Enable ayu colorscheme

" Mod info
let g:Module_ayu_type = "global"
let g:Module_ayu_priority = 5
let g:Module_ayu_requires = ['plug_list']

function! g:Module_ayu_load()
  " Configuration
  let ayucolor="dark"

  " Load
  call plug#load('ayu-vim')

  colorscheme ayu
endfunction
