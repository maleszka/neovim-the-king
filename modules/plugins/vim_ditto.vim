" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Highlight overused words

" Mod info
let g:Module_vim_ditto_type = 'global'
let g:Module_vim_ditto_priority = 0
let g:Module_vim_ditto_requires = ['plug_list']

function! g:Module_vim_ditto_load()
  " ==== Configuration ====
  let g:ditto_min_word_length = 3

  let g:ditto_min_repetitions = 3

  let g:ditto_dir = g:config_home . 'spell'

  " ==== Load ====
  call plug#load('vim-ditto')
endfunction
