" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Dummy package that loads vim-windowswap

" Mod info
let g:Module_vim_windowswap_type = 'global'
let g:Module_vim_windowswap_priority = 4
let g:Module_vim_windowswap_requires = ['plug_list']

function! g:Module_vim_windowswap_load()
  call plug#load('vim-windowswap')
endfunction
