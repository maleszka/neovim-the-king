" Author: Adam Maleszka <adam_maleszka@aol.com>
" Description: Auto-completion written in lua, uses LSP as source

" Mod info
let g:Module_completion_type = 'global'
let g:Module_completion_priority = 3
let g:Module_completion_requires = ['plug_list', 'ultisnips']

function! g:Module_completion_load()
  " Configuration
  let g:completion_enable_snippet = "UltiSnips"
  let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy']
  let g:completion_sorting = 'length'
  let g:completion_confirm_key = ''
  let g:completion_chain_complete_list = {
       \'default' : [
       \  {'complete_items': ['lsp', 'snippet', 'path', 'buffers']},
       \  {'complete_items': ['path', 'buffers']},
       \],
       \'tex' : [
       \  {'complete_items': ['lsp', 'path']}
       \]
       \}

  " Load plugin
  call plug#load('completion-nvim', 'completion-buffers')

  " UTILS
lua << EOF
function _G.check_back_space()
  local col = vim.api.nvim_win_get_cursor(0)[2]
  return (col == 0 or vim.api.nvim_get_current_line():sub(col, col):match('%s')) and true
end
EOF

  " This is a helper function that checks back space before triggering
  " completion.
  function CompletionSmartTrigger()
    if pumvisible()
      return "\<c-n>"
    else
      if v:lua.check_back_space()
        return "\<c-j>"
      else
        lua require'completion'.triggerCompletion()
        return ''
      endif
    endif
  endfunction

  " Mappings
  inoremap <silent> <c-space> <cmd>lua require'completion'.triggerCompletion()<CR>
  inoremap <silent><expr> <CR> pumvisible() ? complete_info()["selected"] != "-1" ? "\<Plug>(completion_confirm_completion)" : "\<c-g>u<CR>" : "\<CR>"
  inoremap <silent><expr> <c-j> CompletionSmartTrigger()
  inoremap <silent><expr> <c-k> pumvisible() ? "\<c-p>" : "\<c-k>"
  imap <silent> <c-s> <Plug>(completion_next_source)
endfunction
