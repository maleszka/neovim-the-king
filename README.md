# King Neovim the First

It is a universally known truth that the King of kings in the Open-Source
editors’ world is Neovim.

King’s impartiality knows no end. His majesty reaches poems and professional
code. No wonder He has a big head. Sometimes it makes him not fit through the
door. That is where this configuration comes in handy, a servant that
occasionally trims or refills King’s brain as needed.

## Module-based model

The core concept of this project is to load and unload specific parts of the
Neovim’s configuration basing on the `filetype` of the current buffer and the
session weight. The management of the module-based system is automatically held
by VimLoader (see [here](https://gitlab.com/maleszka/vload)) to provide
coding-specific and writing-specific settings (and plugins) when needed,
keeping the editor lightweight.

## Faster than light, lighter than a feather

These days, performance does not play as huge a role. Not in this theater.

You can select the weight of modules using the `NVIM_VLOAD_WEIGHT` variable.
Setting this value to 0 will speed up the editor while setting it to 5 will
make it feature-rich. You can now freely copy your configuration whether you
are working on a potato or a computer at work.

Add this to your ~/.bashrc:

```
export NVIM_VLOAD_WEIGHT=3
```

## Installation

Just clone this repository to `~/.config/nvim`. The first time Neovim launches,
it should install plug-vim ([~~the junegunn’s
one~~](https://github.com/junegunn/vim-plug); a [fork](https://gitlab.com/maleszka/vim-plug) of junegunn's one) automatically. After that, type
`:PlugInstall` to install external plugins.

## FAQ

### One module drives me crazy!

This configuration is only a suggested set of VimLoader modules. If you do not
like them but value flexibility and performance, write your collection using
VimLoader (see how it’s simpe: [vload guide](https://gitlab.com/maleszka/vload)).

### Emacs is better

Nope. Maybe Emacs is a king, but is not a king of kings.

## License

This project is released under GPLv3 (full license
[here](https://gnu.org/licenses/gpl-3.0.txt)).
