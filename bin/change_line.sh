#!/bin/bash

if [ "$#" -ne 2 ]; then
  exit 1
fi

# Get list of running instances
SERVERS=$(nvr --serverlist)

# Check each server
for SERVER in $SERVERS; do
  NVIM_FN=$(nvr --servername "$SERVER" --remote-expr "expand('%:p')")

  # The file has been founded
  if [ "$NVIM_FN" -ef "$1" ]; then
    # Change cursor position
    nvr --servername "$SERVER" --remote-expr "cursor($2, col('.'))" &>/dev/null
    exit 0
  fi
done

exit 1
